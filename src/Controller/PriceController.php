<?php

namespace App\Controller;

use App\Entity\CurrencyPair;
use App\Entity\CurrencyPairPrice;
use App\Service\CurrencyPairPriceService;
use App\Service\CurrencyPairService;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

/**
 * @Route("/api/prices")
 */
final class PriceController extends AbstractController
{
    /**
     * @OA\Get(
     *      summary="Provides you list of available crypto currencies",
     *      @OA\Response(
     *          response="200",
     *          description="List of available cryptocurrencies",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     *
     * @Route("/", name="api.currency_pair.list", methods={"GET"})
     *
     * @param CurrencyPairService $pairService
     *
     * @return Response
     */
    public function getCurrencyPairList(CurrencyPairService $pairService): Response
    {
        return $this->json(
            array_map(
                fn(CurrencyPair $pair) => $pair->getSymbol(),
                $pairService->getPreFetchedCurrencies()
            )
        );
    }

    /**
     * @OA\Get(
     *      summary="Provides OHLCV prices for a specific crypto currency pair with possible date range filtration",
     *      @OA\Parameter(
     *          in="path",
     *          name="title",
     *          type="string",
     *          description="Crypto curreny pair title (without /)"
     *      ),
     *      @OA\Parameter(
     *          in="query",
     *          name="date_from",
     *          type="string",
     *          description="Date range bottom value, the format is Y-m-d H:i"
     *      ),
     *      @OA\Parameter(
     *          in="query",
     *          name="date_to",
     *          type="string",
     *          description="Date range top value, the format is Y-m-d H:i"
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Crypto currency pair prices within provided date range",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(
     *                  type="object",
     *                  @OA\Property(
     *                      property="open",
     *                      type="number"
     *                  ),
     *                  @OA\Property(
     *                      property="high",
     *                      type="number"
     *                  ),
     *                  @OA\Property(
     *                      property="low",
     *                      type="number"
     *                  ),
     *                  @OA\Property(
     *                      property="close",
     *                      type="number"
     *                  ),
     *                  @OA\Property(
     *                      property="volume",
     *                      type="number"
     *                  ),
     *                  @OA\Property(
     *                      property="datetime",
     *                      type="string"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Provided crypto currency title not found"
     *      )
     * )
     *
     * @Route("/{title}", name="api.currency_pair.prices", methods={"GET"})
     * @Entity("currencyPair", expr="repository.findByTitle(title)")
     *
     * @param Request $request
     * @param CurrencyPair $currencyPair
     * @param CurrencyPairPriceService $priceService
     *
     * @return Response
     */
    public function getCurrencyPair(Request $request, CurrencyPair $currencyPair, CurrencyPairPriceService $priceService): Response
    {
        if ($errors = $this->validateGetCurrencyPairRequest($request)) {
            return $this->json(
                [
                    'message' => 'Bad request',
                    'data' => $errors,
                ],
                Response::HTTP_BAD_REQUEST,
            );
        }

        $dateFrom = $request->query->get('date_from')
            ? new \DateTimeImmutable($request->query->get('date_from')) : null;
        $dateTo = $request->query->get('date_to')
            ? new \DateTimeImmutable($request->query->get('date_to')) : null;

        $prices = $priceService->getPriceRange($currencyPair, $dateFrom, $dateTo);

        // TODO: Add serialization
        return $this->json(array_map(fn(CurrencyPairPrice $price) => [
            'open' => $price->getOpen(),
            'high' => $price->getHigh(),
            'low' => $price->getLow(),
            'close' => $price->getClose(),
            'volume' => $price->getVolume(),
            'datetime' => $price->getDatetime(),
        ], $prices));
    }

    /**
     * @param Request $request
     *
     * @return array errors
     */
    private function validateGetCurrencyPairRequest(Request $request): array
    {
        $errors = [];
        $validator = Validation::createValidator();
        $data = [
            'date_from' => $request->query->get('date_from'),
            'date_to' => $request->query->get('date_to'),
        ];
        $constraint = new Assert\Collection([
            'date_from' => [
                new Assert\DateTime(['format' => 'Y-m-d H:i']),
            ],
            'date_to' => [
                new Assert\DateTime(['format' => 'Y-m-d H:i']),
            ],
        ]);

        /** @var ConstraintViolationList $violations */
        $violations = $validator->validate($data, $constraint);
        if ($violations->count()) {
            foreach ($violations as $violation) {
                $errors[] = [
                    'message' => $violation->getMessage(),
                    'invalid_value' => $violation->getInvalidValue(),
                    'path' => $violation->getPropertyPath(),
                ];
            }
        }

        return $errors;
    }
}
