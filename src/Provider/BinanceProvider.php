<?php

namespace App\Provider;

use App\Provider\Transformer\BinanceCurrencyPairPriceTransformer;
use App\Provider\Transformer\CurrencyPairPriceTransformerInterface;
use ccxt\BadSymbol;
use ccxt\binance;
use ccxt\Exchange;
use ccxt\ExchangeError;
use ccxt\NotSupported;
use Psr\Log\LoggerInterface;

final class BinanceProvider extends AbstractProvider
{
    /**
     * @var Exchange
     */
    private Exchange $exchange;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param LoggerInterface $logger
     * @param BinanceCurrencyPairPriceTransformer $transformer
     */
    public function __construct(LoggerInterface $logger, BinanceCurrencyPairPriceTransformer $transformer)
    {
        $this->logger = $logger;
        $this->setCurrencyPairPriceTransformer($transformer);

        try {
            $this->exchange = new binance([
                'apiKey' => getenv('BINANCE_API_KEY'),
                'secret' => getenv('BINANCE_API_SECRET'),
                'timeout' => 10000,
                'enableRateLimit' => true,
            ]);
            $this->available = true;
        } catch (ExchangeError $exception) {
            $this->logger->error(
                'Cannot initiate Binance exchange provider, error {code}: {message}', [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ]
            );
            $this->available = false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOHLCVPrices(string $symbol, \DateTimeImmutable $since, string $timeFrame = '1h'): array
    {
        if (!$this->isAvailable()) {
            return [];
        }

        try {
            $symbolPriceData = $this->exchange->fetch_ohlcv($symbol, $timeFrame, $since->getTimestamp() * 1000);
        } catch (NotSupported $exception) {
            $this->logger->warning('Binance exchange does not support fetching OHLCV data');

            return [];
        } catch (BadSymbol $exception) {
            $this->logger->warning('Binance exchange does not support symbol "{symbol}"', ['symbol' => $symbol]);

            return [];
        } catch (\Throwable $exception) {
            $this->logger->error(
                'Binance: unknown exception during fetching OHLCV data for symbol "{symbol}", error {code}: "{message}"',
                [
                    'symbol' => $symbol,
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ]
            );

            return [];
        }

        $currencyPairPrices = [];
        foreach ($symbolPriceData as $symbolPrice) {
            $currencyPairPrices[] = $this->currencyPairPriceTransformer->transform($symbolPrice);
        }

        return $currencyPairPrices;
    }

    /**
     * @param CurrencyPairPriceTransformerInterface $transformer
     */
    protected function setCurrencyPairPriceTransformer(CurrencyPairPriceTransformerInterface $transformer): void
    {
        $this->currencyPairPriceTransformer = $transformer;
    }
}
