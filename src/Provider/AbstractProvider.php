<?php

namespace App\Provider;

use App\Provider\Transformer\CurrencyPairPriceTransformerInterface;

abstract class AbstractProvider implements ProviderInterface
{
    /**
     * @var bool
     */
    protected bool $available;

    /**
     * @var CurrencyPairPriceTransformerInterface
     */
    protected CurrencyPairPriceTransformerInterface $currencyPairPriceTransformer;

    /**
     * {@inheritDoc}
     */
    public function isAvailable(): bool
    {
        return $this->available;
    }

    /**
     * @param CurrencyPairPriceTransformerInterface $transformer
     */
    abstract protected function setCurrencyPairPriceTransformer(CurrencyPairPriceTransformerInterface $transformer): void;
}
