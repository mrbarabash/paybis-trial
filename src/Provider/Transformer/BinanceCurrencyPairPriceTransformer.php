<?php

namespace App\Provider\Transformer;

use App\Entity\CurrencyPairPrice;

class BinanceCurrencyPairPriceTransformer implements CurrencyPairPriceTransformerInterface
{
    /**
     * {@inheritDoc}
     */
    public function transform(array $priceData): CurrencyPairPrice
    {
        list($datetime, $open, $high, $low, $close, $volume) = $priceData;

        $price = new CurrencyPairPrice();
        $price->setDatetime((new \DateTimeImmutable())->setTimestamp($datetime / 1000));
        $price->setOpen($open);
        $price->setHigh($high);
        $price->setLow($low);
        $price->setClose($close);
        $price->setVolume($volume);

        return $price;
    }
}
