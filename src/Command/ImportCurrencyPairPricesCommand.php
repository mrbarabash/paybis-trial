<?php

namespace App\Command;

use App\Service\ProviderService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ImportCurrencyPairPricesCommand extends Command
{
    protected static $defaultName = 'app:import:currency-prices';

    /**
     * @var ProviderService
     */
    private ProviderService $providerService;

    /**
     * @param ProviderService $providerService
     */
    public function __construct(ProviderService $providerService)
    {
        $this->providerService = $providerService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Import crypto currency prices')
            ->addArgument(
                'symbols',
                InputArgument::IS_ARRAY | InputArgument::OPTIONAL,
                'Provide crypto currency pair symbols like BTC/USDT, separate with a space',
                ['BTC/USDT']
            )
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $symbols = $input->getArgument('symbols');

        $this->providerService->importData($symbols);

        return 0;
    }
}
