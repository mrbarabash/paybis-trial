<?php

namespace App\Service;

use App\Entity\CurrencyPair;
use App\Repository\CurrencyPairRepository;

final class CurrencyPairService
{
    /**
     * @var CurrencyPairRepository
     */
    private CurrencyPairRepository $repository;

    /**
     * @param CurrencyPairRepository $repository
     */
    public function __construct(CurrencyPairRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return CurrencyPair[]
     */
    public function getPreFetchedCurrencies(): array
    {
        return $this->repository->findAll();
    }
}
