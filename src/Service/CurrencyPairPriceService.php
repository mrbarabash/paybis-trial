<?php

namespace App\Service;

use App\Entity\CurrencyPair;
use App\Entity\CurrencyPairPrice;
use App\Repository\CurrencyPairPriceRepository;

class CurrencyPairPriceService
{
    /**
     * @var CurrencyPairPriceRepository
     */
    private CurrencyPairPriceRepository $repository;

    /**
     * @param CurrencyPairPriceRepository $repository
     */
    public function __construct(CurrencyPairPriceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CurrencyPair $currencyPair
     * @param \DateTimeImmutable|null $dateFrom
     * @param \DateTimeImmutable|null $dateTo
     *
     * @return CurrencyPairPrice[]
     */
    public function getPriceRange(CurrencyPair $currencyPair, ?\DateTimeImmutable $dateFrom, ?\DateTimeImmutable $dateTo): array
    {
        if (!$dateFrom) {
            $dateFrom = new \DateTimeImmutable('-7 days');
        }
        if (!$dateTo) {
            $dateTo = new \DateTimeImmutable();
        }

        return $this->repository->findInDateRange($currencyPair, $dateFrom, $dateTo);
    }
}
