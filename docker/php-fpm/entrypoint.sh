#!/usr/bin/env bash

composer install --no-dev

php bin/console cache:warmup -e prod
php bin/console doctrine:migrations:migrate --no-interaction -vvv

# Init data load
php bin/console app:import:currency-prices BTC/USDT BTC/EUR BTC/RUB

php-fpm -F
