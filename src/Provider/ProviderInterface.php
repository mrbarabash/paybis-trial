<?php

namespace App\Provider;

use App\Entity\CurrencyPairPrice;

interface ProviderInterface
{
    /**
     * @param string $symbol
     * @param \DateTimeImmutable $since
     * @param string $timeFrame
     *
     * @return CurrencyPairPrice[]
     */
    public function getOHLCVPrices(string $symbol, \DateTimeImmutable $since, string $timeFrame = '1h'): array ;

    /**
     * @return bool
     */
    public function isAvailable(): bool;
}
