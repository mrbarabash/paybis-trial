# Crypto Currencies Chart API

API provides you an ability to retrieve and build (using separate frontend web or mobile application) crypto currency [OHLCV prices]([https://en.wikipedia.org/wiki/Open-high-low-close_chart](https://en.wikipedia.org/wiki/Open-high-low-close_chart)) with date range filtration.


## Deployment
Deployment for this project is supported by docker containers.


To run it:
```bash
cd docker && docker-compose up -d
```

### Custom Configuration


#### Host Name
Host name is hardcoded now in `docker-compose.yaml` file, it could be changed directly there, moved to environment variable or specified during docker build command run.

Do not forget to update your OS hosts file.

Default host name is `crypto-chart.local`.

#### MySQL
To customise MySQL connection please see the following:

1. Copy .env file in the root directory to .env.local

2. Update string `DATABASE_URL=mysql://db_user:db_password@mysql/db_name?serverVersion=8.0` with your parameters.

3. Inside `docker/docker-compose.yaml` file update MySQL container environment variables.

4. Finally, do the changes in the init db SQL script `docker/db/init.sql`.

## Documentation
While docker containers are running, you could find the project's documentation using the URL `http://<hostname>/api/doc`.


## TESTS
To be done later.

