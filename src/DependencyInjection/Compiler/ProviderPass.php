<?php

namespace App\DependencyInjection\Compiler;

use App\Service\ProviderService;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

final class ProviderPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(ProviderService::class)) {
            return;
        }

        $providerService = $container->findDefinition(ProviderService::class);
        $providers = $this->findAndSortTaggedServices('app.provider', $container);

        foreach ($providers as $reference) {
            $providerService->addMethodCall('addProvider', [$reference]);
        }
    }
}
