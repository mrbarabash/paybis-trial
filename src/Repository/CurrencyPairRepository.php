<?php

namespace App\Repository;

use App\Entity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class CurrencyPairRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entity\CurrencyPair::class);
    }

    /**
     * @param Entity\CurrencyPair $currencyPair
     * @param bool $withFlush
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Entity\CurrencyPair $currencyPair, $withFlush = true)
    {
        $this->_em->persist($currencyPair);
        if ($withFlush) {
            $this->_em->flush();
        }
    }

    /**
     * @param string $title
     *
     * @return Entity\CurrencyPair|null
     */
    public function findByTitle(string $title): ?Entity\CurrencyPair
    {
        return $this->findOneBy(['title' => strtolower($title)]);
    }
}
