<?php

namespace App\Provider\Transformer;

use App\Entity\CurrencyPairPrice;

interface CurrencyPairPriceTransformerInterface
{
    /**
     * @param array $priceData
     *
     * @return CurrencyPairPrice
     */
    public function transform(array $priceData): CurrencyPairPrice;
}
