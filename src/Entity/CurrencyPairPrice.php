<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyPairPriceRepository")
 * @ORM\Table(indexes={@ORM\Index(name="datetime_idx", columns={"datetime"})})
 */
class CurrencyPairPrice
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private int $id = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CurrencyPair", inversedBy="prices")
     *
     * @var CurrencyPair|null
     */
    private ?CurrencyPair $currencyPair = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     * @var \DateTimeImmutable|null
     */
    private ?\DateTimeImmutable $datetime = null;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    private float $open = 0.0;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    private float $high = 0.0;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    private float $low = 0.0;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    private float $close = 0.0;

    /**
     * @ORM\Column(type="float")
     *
     * @var float
     */
    private float $volume = 0.0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return CurrencyPair|null
     */
    public function getCurrencyPair(): ?CurrencyPair
    {
        return $this->currencyPair;
    }

    /**
     * @param CurrencyPair $currencyPair
     */
    public function setCurrencyPair(CurrencyPair $currencyPair): void
    {
        $this->currencyPair = $currencyPair;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getDatetime(): ?\DateTimeImmutable
    {
        return $this->datetime;
    }

    /**
     * @param \DateTimeImmutable $datetime
     */
    public function setDatetime(\DateTimeImmutable $datetime): void
    {
        $this->datetime = $datetime;
    }

    /**
     * @return float
     */
    public function getOpen(): float
    {
        return $this->open;
    }

    /**
     * @param float $open
     */
    public function setOpen(float $open): void
    {
        $this->open = $open;
    }

    /**
     * @return float
     */
    public function getHigh(): float
    {
        return $this->high;
    }

    /**
     * @param float $high
     */
    public function setHigh(float $high): void
    {
        $this->high = $high;
    }

    /**
     * @return float
     */
    public function getLow(): float
    {
        return $this->low;
    }

    /**
     * @param float $low
     */
    public function setLow(float $low): void
    {
        $this->low = $low;
    }

    /**
     * @return float
     */
    public function getClose(): float
    {
        return $this->close;
    }

    /**
     * @param float $close
     */
    public function setClose(float $close): void
    {
        $this->close = $close;
    }

    /**
     * @return float
     */
    public function getVolume(): float
    {
        return $this->volume;
    }

    /**
     * @param float $volume
     */
    public function setVolume(float $volume): void
    {
        $this->volume = $volume;
    }
}
