<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyPairRepository")
 */
class CurrencyPair
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private int $id = 0;

    /**
     * @ORM\Column(length=21)
     *
     * @var string
     */
    private string $symbol = '';

    /**
     * @ORM\Column(length=20)
     *
     * @var string
     */
    private string $title = '';

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     *
     * @var \DateTimeImmutable|null
     */
    private ?\DateTimeImmutable $lastUpdate = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CurrencyPairPrice", mappedBy="currencyPair", cascade={"persist", "remove"})
     *
     * @var Collection|null
     */
    private ?Collection $prices = null;

    public function __construct()
    {
        $this->prices = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     */
    public function setSymbol(string $symbol): void
    {
        $this->symbol = $symbol;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return CurrencyPairPrice[]|Collection
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param CurrencyPairPrice[]|Collection $prices
     */
    public function setPrices($prices): void
    {
        $this->prices = $prices;
    }

    /**
     * @param CurrencyPairPrice $price
     */
    public function addPrice(CurrencyPairPrice $price): void
    {
        $price->setCurrencyPair($this);
        $this->prices->add($price);
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getLastUpdate(): ?\DateTimeImmutable
    {
        return $this->lastUpdate;
    }

    /**
     * @param \DateTimeImmutable $lastUpdate
     */
    public function setLastUpdate(\DateTimeImmutable $lastUpdate): void
    {
        $this->lastUpdate = $lastUpdate;
    }
}
