<?php

namespace App\Repository;

use App\Entity;
use App\Entity\CurrencyPair;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class CurrencyPairPriceRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entity\CurrencyPairPrice::class);
    }

    /**
     * @param CurrencyPair $currencyPair
     * @param \DateTimeImmutable $dateFrom
     * @param \DateTimeImmutable $dateTo
     *
     * @return array
     */
    public function findInDateRange(CurrencyPair $currencyPair, \DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo): array
    {
        return $this->createQueryBuilder('cpp')
            ->where('cpp.currencyPair = :pair')
            ->setParameter('pair', $currencyPair)
            ->andWhere('cpp.datetime BETWEEN :dateFrom AND :dateTo')
            ->setParameter('dateFrom', $dateFrom)
            ->setParameter('dateTo', $dateTo)
            ->getQuery()
            ->getResult()
        ;
    }
}
