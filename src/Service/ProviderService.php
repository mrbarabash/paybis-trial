<?php

namespace App\Service;

use App\DependencyInjection\Compiler\ProviderPass;
use App\Entity\CurrencyPair;
use App\Provider\ProviderInterface;
use App\Repository\CurrencyPairPriceRepository;
use App\Repository\CurrencyPairRepository;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;

final class ProviderService
{
    private const DEFAULT_SINCE_DATETIME_INTERVAL = '-7 days';

    /**
     * @var ProviderInterface[]
     */
    private array $providers = [];

    /**
     * @var CurrencyPairRepository
     */
    private CurrencyPairRepository $currencyPairRepository;

    /**
     * @var CurrencyPairPriceRepository
     */
    private CurrencyPairPriceRepository $currencyPairPriceRepository;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param CurrencyPairRepository $currencyPairRepository
     * @param CurrencyPairPriceRepository $currencyPairPriceRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        CurrencyPairRepository $currencyPairRepository,
        CurrencyPairPriceRepository $currencyPairPriceRepository,
        LoggerInterface $logger
    ) {
        $this->currencyPairRepository = $currencyPairRepository;
        $this->currencyPairPriceRepository = $currencyPairPriceRepository;
        $this->logger = $logger;
    }

    /**
     * @param ProviderInterface $provider
     */
    public function addProvider(ProviderInterface $provider): void
    {
        $this->providers[] = $provider;
    }

    /**
     * Method called in compiler pass ProviderPass
     * @see ProviderPass
     *
     * @param array $symbols
     */
    public function importData(array $symbols): void
    {
        foreach ($symbols as $symbol) {
            $this->importSymbolDataWithFirstAvailableExchange(strtolower($symbol));
        }
    }

    /**
     * @param string $symbol
     */
    private function importSymbolDataWithFirstAvailableExchange(string $symbol)
    {
        if (!$availableProviders = $this->getAvailableProviders()) {
            $this->logger->warning('There are no available exchanges at the moment');
            return;
        }

        // First, check do we have provided pair in our database
        if (!$currencyPair = $this->currencyPairRepository->findOneBy(['symbol' => $symbol])) {
            $currencyPair = new CurrencyPair();
            $currencyPair->setSymbol($symbol);
            $currencyPair->setTitle(str_replace("/", "", $symbol));
        }

        $importSince = $currencyPair->getLastUpdate() ?: new \DateTimeImmutable(self::DEFAULT_SINCE_DATETIME_INTERVAL);

        // Fetch symbol prices from the first available provider
        $currencyPairPrices = [];
        $lastUpdate = new \DateTimeImmutable();
        foreach ($availableProviders as $provider) {
            if ($currencyPairPrices = $provider->getOHLCVPrices(strtoupper($symbol), $importSince)) {
                break;
            }
        }

        if (!$currencyPairPrices) {
            // No fresh data
            return;
        }

        // Append prices data to CurrencyPair object
        foreach ($currencyPairPrices as $price) {
            $currencyPair->addPrice($price);
        }

        $currencyPair->setLastUpdate($lastUpdate);

        try {
            $this->currencyPairRepository->save($currencyPair);
        } catch (ORMException $exception) {
            $this->logger->error(
                'Cannot save currency pair prices for symbol "{symbol}" fetched from Provider "{provider}"',
                [
                    'symbol' => $symbol,
                    'provider' => get_class($provider),
                ]
            );
        }
    }

    /**
     * @return ProviderInterface[]
     */
    private function getAvailableProviders(): array
    {
        return array_filter($this->providers, fn(ProviderInterface $provider) => $provider->isAvailable());
    }
}
